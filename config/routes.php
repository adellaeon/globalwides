<?php

return [
    'index'     => 'default/index',
    'search'     => 'default/search',
    'data'   => 'ajax/data',
];