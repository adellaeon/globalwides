function getData(text) {
    $.ajax({
        async: true,
        type: "POST",
        url: "data",
        error: function () {
            alert('Ошибка обработки данных');
        },
        success: function (data) {
            var result = $.parseJSON(data);
            if (!result.error && result.data) {
                $('#autocomplete').show();
                $('#autocomplete').html(result.data);
            } else {
                $('#autocomplete').html("");
                $('#autocomplete').hide();
            }
        },
        data: {
            text: text
        }
    });
}

$(document).ready(function () {
    var city = "Город";
    var hotel = "Гостиница";
    var region = "Регион";

    $('#search').focus();

    $('#search').keyup(function (event) {
        switch (event.keyCode) {
            case 13:
            case 27:
            case 38:
            case 40:
                break;
            default:
                var chars = $(this).val().length;
                if (chars > 1) {
                    var text = $(this).val();
                    getData(text);
                } else {
                    $('#autocomplete').html('');
                    $('#autocomplete').hide();
                }
        }
    });

    $('#autocomplete').on('click', 'li', function () {
        var type = $(this).attr('class');
        var text = $(this).text();
        text = text.replace(hotel + ':', '').replace(city + ':', '').replace(region + ':', '').replace(/^\s*/, '').replace(/\s*$/, '');
        var url = '';
        if (type == 'city') {
            text = city + " " + text;
            url = "/search?city=1&city_id=" + $(this).attr('id');
        } else if (type == 'hotel') {
            text = hotel + " " + text;
            url = "/search?hotel=1&hotel_id=" + $(this).attr('id');
        } else if (type == 'region') {
            url = "/search?region=1&region_id=" + $(this).attr('id');
        }

        $('form').attr('action', url);
        $('#search').val(text);
        $('#search').focus();
        $('#autocomplete').hide();
    });

    $('html').click(function () {
        $('#autocomplete').hide();
    });

    $('#search').click(function (event) {
        $('#autocomplete').show();
        event.stopPropagation();
    });
});