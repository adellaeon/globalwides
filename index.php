<?php
namespace app;

define('ROOT', dirname(__FILE__));

require_once(ROOT . '/config/config.php');
require_once(ROOT . '/config/db.php');
require_once(ROOT . '/lib/Router.php');
require_once(ROOT . '/vendor/autoload.php');

use app\lib\Router;

$routes = ROOT . '/config/routes.php';

(new Router($routes))->run();