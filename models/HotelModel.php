<?php

namespace app\models;

/**
 * HotelModel Class
 *
 * @version 0.1.0
 */

use app\lib\Model;

class HotelModel extends Model
{
    public $id;

    public $name;

    public $city_id;

    public function fieldsTable()
    {
        return [
            'id'      => 'Id',
            'name'    => 'Название отеля',
            'city_id' => 'Id города',
        ];
    }
}