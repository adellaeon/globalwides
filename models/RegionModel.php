<?php

namespace app\models;

/**
 * RegionModel Class
 *
 * @version 0.1.0
 */

use app\lib\Model;

class RegionModel extends Model
{
    public $id;

    public $name;

    public function fieldsTable()
    {
        return [
            'id'   => 'Id',
            'name' => 'Название региона',
        ];
    }
}