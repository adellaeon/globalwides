<?php

namespace app\models;

/**
 * CityModel Class
 *
 * @version 0.1.0
 */

use app\lib\Model;

class CityModel extends Model
{
    public $id;

    public $name;

    public $region_id;

    public function fieldsTable()
    {
        return [
            'id'        => 'Id',
            'name'      => 'Название города',
            'region_id' => 'Id региона',
        ];
    }
}