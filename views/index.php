<h2>Поиск гостиниц, городов и регионов Украины</h2>

<div class="row">
    <form method="post" action="/search" enctype="multipart/form-data">
        <div class="col-sm-9 col-md-9 col-lg-9">
            <div class="form-group">
                <input type="text" class="form-control" name="search" id="search" autocomplete="off"
                       placeholder="Введите название города или гостиницы или региона">
                <div id="autocomplete" style="display: none"></div>
            </div>
        </div>
        <div class="col-sm-3 col-md-3 col-lg-3">
            <button type="submit" class="btn btn-success">Submit</button>
        </div>
    </form>
</div>