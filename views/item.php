<h2>Результат поиска</h2>

<div class="row">
    <div class="col-xs-12">
        <?php if(!empty($item)): ?>
            <div class="alert alert-success" role="alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                Ваш поиск <b><?=$item?></b> успешено завершен! Вернуться на страницу <a href="/" class="alert-link">поиска</a>.
            </div>
            <?php echo $data; ?>
        <?php else: ?>
            <div class="alert alert-warning" role="alert">
                По вашему запросу ничего не найдено! Вернитесь на страницу <a href="/" class="alert-link">поиска</a>.
            </div>
        <?php endif; ?>
    </div>
</div>
