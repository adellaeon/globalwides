<?php
namespace app\controllers;

/**
 * DefaultController Class
 *
 * Responsible for displaying information.
 *
 * @version 0.1.0
 */

use app\lib\Controller;
use app\models\CityModel;
use app\models\HotelModel;
use app\models\RegionModel;

class DefaultController extends Controller
{
    /**
     * Action `Index`
     *
     * @return $this
     */

    public function actionIndex()
    {
        return $this->view->render('index');
    }

    /**
     * Action `Search`
     *
     * Displays search information
     *
     * @var string $data
     *
     * @return $this
     */
    public function actionSearch()
    {
        $item = false;
        $data = '';
        if($_GET && count($_GET) > 0) {
            if(array_key_exists('region', $_GET)) {
                $item = 'региона';
                $data = $this->getInfoRegionById((int)$_GET['region_id']);
            }
            if(array_key_exists('city', $_GET)) {
                $item = 'города';
                $data = $this->getInfoCityById((int)$_GET['city_id']);
            }
            if(array_key_exists('hotel', $_GET)) {
                $item = 'гостиницы';
                $data = $this->getInfoHotelById((int)$_GET['hotel_id']);
            }
        }

        return $this->view->render('item', ['item' => $item, 'data' => $data]);
    }

    /**
     * method `getInfoRegionById`
     *
     * returns information about the region
     *
     * @var string $data
     *
     * @return $data
     */
    protected function getInfoRegionById($id)
    {
        $data = '';
        $modelRegion = new RegionModel();
        $region = $modelRegion->getRowById($id);

        if($region) {
            $data = REGION . ': ' . $region['name'] . '<br/>';
        }

        return $data;
    }

    /**
     * method `getInfoCityById`
     *
     * returns information about the city
     *
     * @var string $data
     *
     * @return $data
     */
    protected function getInfoCityById($id)
    {
        $data = '';
        $modelCity = new CityModel();
        $city = $modelCity->getRowById($id);

        if($city) {
            $data = CITY . ': ' . $city['name'] . '<br/>';
            $data .= $this->getInfoRegionById($city['region_id']);
        }

        return $data;
    }

    /**
     * method `getInfoHotelById`
     *
     * returns information about the hotel
     *
     * @var string $data
     *
     * @return $data
     */
    protected function getInfoHotelById($id)
    {
        $data = '';
        $modelHotel = new HotelModel();
        $hotel = $modelHotel->getRowById($id);

        if($hotel) {
            $data = HOTEL . ': ' . $hotel['name'] . '<br/>';
            $data .= $this->getInfoCityById($hotel['city_id']);
        }

        return $data;
    }
}