<?php

namespace app\controllers;

/**
 * AjaxController Class
 *
 * Responsible for ajax request on the search text
 *
 * @version 0.1.0
 */

use app\models\CityModel;
use app\models\HotelModel;
use app\models\RegionModel;

class AjaxController
{
    /**
     * Action `Data`
     *
     * @return $this json
     */

    public function actionData()
    {
        $error = false;
        if(array_key_exists('text', $_POST)) {
            $text = $_POST['text'] ? trim($_POST['text']) : '';
        }

        if(!$text) {
            $error = true;
        }

        $text = "%" . $text . "%";

        $modelHotel = new HotelModel(['where' => " name LIKE '" . $text . "'", 'order' => 'name ASC']);
        $hotel = $modelHotel->getAllRows();

        $modelCity = new CityModel(['where' => " name LIKE '" . $text . "'", 'order' => 'name ASC']);
        $city = $modelCity->getAllRows();

        $modelRegion = new RegionModel(['where' => " name LIKE '" . $text . "'", 'order' => 'name ASC']);
        $region = $modelRegion->getAllRows();

        $data = '';

        if($city || $hotel || $region) {
            $data = "<ul>";

            if($city) {
                $data .= $this->getData($city, 'city', CITY);
            }

            if($hotel) {
                $data .= $this->getData($hotel, 'hotel', HOTEL);
            }

            if($region) {
                $data .= $this->getData($region, 'region', REGION);
            }
            $data .= "</ul>";
        }

        echo json_encode(['data' => $data, 'error' => $error]);
    }

    /**
     * method `getData`
     *
     * returns data string
     *
     * @var array  $items
     * @var string $class
     * @var string $title
     * @var string $data
     *
     * @return $data
     */
    protected function getData($items, $class, $title)
    {
        $data = '';
        $i = 0;
        foreach($items as $item) {
            $data .= "<li class='" . $class . "' id='" . $item['id'] . "'>";
            if($i == 0) {
                $data .= "<div class='title'>" . $title . ": &nbsp;</div>";
            } else {
                $data .= "<div class='item'></div>";
            }
            $data .= $item['name'] . "</li>";
            $i++;
        }

        return $data;
    }
}