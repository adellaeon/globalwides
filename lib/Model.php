<?php

namespace app\lib;

/**
 * Model Class
 *
 * @version 0.1.0
 */

class Model
{
    /**
     * @var string $db
     * @var string $table
     * @var string $dataResult
     */

    protected $db;

    protected $table;

    private $dataResult;

    public function __construct($select = false)
    {
        //object BD connection
        global $dbObject;
        $this->db = $dbObject;

        //table name
        $modelFullName = explode('\\', get_class($this));
        $modelName = end($modelFullName);

        $arrExp = explode('Model', $modelName);
        $tableName = strtolower($arrExp[0]);
        $this->table = $tableName;

        // processing request if necessary
        $sql = $this->_getSelect($select);

        if($sql) {
            $this->_getResult("SELECT * FROM $this->table" . $sql);
        }
    }

    /**
     * Method `getTableName`
     *
     * @return $this table name
     */

    public function getTableName()
    {
        return $this->table;
    }

    /**
     * Method `getAllRows`
     *
     * @return $this all rows from model
     */
    public function getAllRows()
    {
        if(!isset($this->dataResult) OR empty($this->dataResult)) {
            return false;
        }

        return $this->dataResult;
    }

    /**
     * Method `getOneRow`
     *
     * @return $this one row from model
     */
    public function getOneRow()
    {
        if(!isset($this->dataResult) OR empty($this->dataResult)) {
            return false;
        }

        return $this->dataResult[0];
    }

    /**
     * Method `fetchOne`
     *
     * Extract one row from the database
     *
     * @return true
     */
    public function fetchOne()
    {
        if(!isset($this->dataResult) OR empty($this->dataResult)) {
            return false;
        }

        foreach($this->dataResult[0] as $key => $val) {
            $this->$key = $val;
        }

        return true;
    }

    /**
     * Method `getRowById`
     *
     * @var int $id
     *
     * @return $row
     */
    public function getRowById($id)
    {
        try {
            $db = $this->db;
            $stmt = $db->query("SELECT * from $this->table WHERE id = $id");
            $row = $stmt->fetch();
        } catch(PDOException $e) {
            echo $e->getMessage();
            exit;
        }

        return $row;
    }

    /**
     * Method `getLastRow`
     *
     * @return $row
     */
    public function getLastRow()
    {
        try {
            $db = $this->db;
            $stmt = $db->query("SELECT * from $this->table ORDER BY id DESC LIMIT 1");
            $row = $stmt->fetch();
        } catch(PDOException $e) {
            echo $e->getMessage();
            exit;
        }

        return $row;
    }

    /**
     * Method `save`
     *
     * @return $result
     */
    public function save()
    {
        $arrayAllFields = array_keys($this->fieldsTable());
        $arraySetFields = [];
        $arrayData = [];

        foreach($arrayAllFields as $field) {
            if(!empty($this->$field)) {
                $arraySetFields[] = $field;
                $arrayData[] = $this->$field;
            }
        }

        $forQueryFields = implode(', ', $arraySetFields);
        $rangePlace = array_fill(0, count($arraySetFields), '?');
        $forQueryPlace = implode(', ', $rangePlace);

        try {
            $db = $this->db;
            $stmt = $db->prepare("INSERT INTO $this->table ($forQueryFields) values ($forQueryPlace)");
            $result = $stmt->execute($arrayData);
        } catch(PDOException $e) {
            echo 'Error : ' . $e->getMessage();
            echo '<br/>Error sql : ' . "'INSERT INTO $this->table ($forQueryFields) values ($forQueryPlace)'";
            exit();
        }

        return $result;
    }

    /**
     * Private method `_getSelect`
     *
     * compiling a query to the database
     *
     * @return $querySql
     */
    private function _getSelect($select)
    {
        if(is_array($select)) {
            $allQuery = array_keys($select);
            array_walk($allQuery, function(&$val) {
                $val = strtoupper($val);
            });

            $querySql = "";
            if(in_array("WHERE", $allQuery)) {
                foreach($select as $key => $val) {
                    if(strtoupper($key) == "WHERE") {
                        $querySql .= " WHERE " . $val;
                    }
                }
            }

            if(in_array("GROUP", $allQuery)) {
                foreach($select as $key => $val) {
                    if(strtoupper($key) == "GROUP") {
                        $querySql .= " GROUP BY " . $val;
                    }
                }
            }

            if(in_array("ORDER", $allQuery)) {
                foreach($select as $key => $val) {
                    if(strtoupper($key) == "ORDER") {
                        $querySql .= " ORDER BY " . $val;
                    }
                }
            }

            if(in_array("LIMIT", $allQuery)) {
                foreach($select as $key => $val) {
                    if(strtoupper($key) == "LIMIT") {
                        $querySql .= " LIMIT " . $val;
                    }
                }
            }

            return $querySql;
        }

        return false;
    }

    /**
     * Private method `_getResult`
     *
     * Execute a query to the database
     *
     * @return $rows
     */
    private function _getResult($sql = null)
    {
        try {
            $db = $this->db;
            $stmt = $db->query($sql);
            $rows = $stmt->fetchAll();
            $this->dataResult = $rows;
        } catch(PDOException $e) {
            echo $e->getMessage();
            exit;
        }

        return $rows;
    }

    /**
     * Method `deleteBySelect`
     *
     * delete rows from the database by condition
     *
     * @return $result
     */

    public function deleteBySelect($select)
    {
        $sql = $this->_getSelect($select);

        try {
            $db = $this->db;
            $result = $db->exec("DELETE FROM $this->table " . $sql);
        } catch(PDOException $e) {
            echo 'Error : ' . $e->getMessage();
            echo '<br/>Error sql : ' . "'DELETE FROM $this->table " . $sql . "'";
            exit();
        }

        return $result;
    }

    /**
     * Method `deleteRow`
     *
     * delete row from the database by condition
     *
     * @return $result
     */
    public function deleteRow()
    {
        $arrayAllFields = array_keys($this->fieldsTable());

        array_walk($arrayAllFields, function(&$val) {
            $val = strtoupper($val);
        });

        if(in_array('ID', $arrayAllFields)) {
            try {
                $db = $this->db;
                $result = $db->exec("DELETE FROM $this->table WHERE `id` = $this->id");

                foreach($arrayAllFields as $one) {
                    unset($this->$one);
                }
            } catch(PDOException $e) {
                echo 'Error : ' . $e->getMessage();
                echo '<br/>Error sql : ' . "'DELETE FROM $this->table WHERE `id` = $this->id'";
                exit();
            }
        } else {
            echo "ID table `$this->table` not found!";
            exit;
        }

        return $result;
    }

    /**
     * Method `update`
     *
     * update row from the database by ID
     *
     * @return $result
     */
    public function update()
    {
        $arrayAllFields = array_keys($this->fieldsTable());
        $arrayForSet = [];

        foreach($arrayAllFields as $field) {
            if(!empty($this->$field)) {
                if(strtoupper($field) != 'ID') {
                    $arrayForSet[] = $field . ' = "' . $this->$field . '"';
                } else {
                    $whereID = $this->$field;
                }
            }
        }

        if(!isset($arrayForSet) OR empty($arrayForSet)) {
            echo "Array data table `$this->table` empty!";
            exit;
        }

        if(!isset($whereID) OR empty($whereID)) {
            echo "ID table `$this->table` not found!";
            exit;
        }

        $strForSet = implode(', ', $arrayForSet);

        try {
            $db = $this->db;
            $stmt = $db->prepare("UPDATE $this->table SET $strForSet WHERE `id` = $whereID");
            $result = $stmt->execute();
        } catch(PDOException $e) {
            echo 'Error : ' . $e->getMessage();
            echo '<br/>Error sql : ' . "'UPDATE $this->table SET $strForSet WHERE `id` = $whereID'";
            exit();
        }

        return $result;
    }
}