<?php

namespace app\lib;

/**
 * Controller Class
 *
 * @version 0.1.0
 */

class Controller
{
    /**
     * @var string $view View
     */

    protected $view;

    function __construct()
    {
        $this->view = new View();
    }
}