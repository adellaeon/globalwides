<?php

namespace app\lib;

/**
 * Controller Class
 *
 * @version 0.1.0
 */

class Router
{
    /**
     * @var string $routes
     */

    private $routes;

    function __construct($routesPath)
    {
        //get and include config file.
        $this->routes = include($routesPath);
    }

    /**
     * Method `getURI`
     *
     * GET URI
     *
     * @return $uri
     */
    function getURI()
    {
        if(!empty($_SERVER['REQUEST_URI'])) {
            $segments = parse_url($_SERVER['REQUEST_URI']);
            $uri = trim($segments['path'], '/');

            return $uri ? $uri : 'index';
        }
    }

    /**
     * Method `run`
     *
     * @return $this
     */
    function run()
    {
        $uri = $this->getURI();

        //apply the rules from config
        foreach($this->routes as $pattern => $route) {
            //if  rule has worked
            if(preg_match("~$pattern~", $uri)) {
                //Get the internal path from an external path according to the rule
                $internalRoute = preg_replace("~$pattern~", $route, $uri);

                //Break path into segments
                $segments = explode('/', $internalRoute);
                $controllerName = ucfirst(array_shift($segments)) . 'Controller';
                $action = 'action' . ucfirst(array_shift($segments));
                $params = $segments;
                $controllerName = "app\\controllers\\" . $controllerName;

                $controller = new $controllerName();

                //If the required class controller is not loaded or there is no necessary method return Error
                if(!is_callable([$controller, $action])) {
                    print_r('Error');

                    return;
                }

                call_user_func_array([$controller, $action], $params);
            }
        }

        return;
    }
}