<?php

namespace app\lib;

/**
 * Controller Class
 *
 * @version 0.1.0
 */

class View
{
    /**
     * Method `fetchPartial`
     *
     * Get a rendered template with parameters $ params
     *
     * @return ob_get_clean
     */
    public function fetchPartial($template, $params = [])
    {
        extract($params);
        ob_start();
        include(ROOT . '/views/' . $template . '.php');

        return ob_get_clean();
    }

    /**
     * Method `renderPartial`
     *
     * Output a rendered template with parameters $ params
     *
     * @return $this
     */
    public function renderPartial($template, $params = [])
    {
        echo $this->fetchPartial($template, $params);
    }

    /**
     * Method `fetch`
     *
     * get rendered into  $content layout
     * template with parameters $ params
     *
     * @return $this
     */
    public function fetch($template, $params = [])
    {
        $content = $this->fetchPartial($template, $params);

        return $this->fetchPartial('layout', ['content' => $content]);
    }

    /**
     * Method `render`
     *
     * print template with parameters $ params
     *
     * @return $this
     */

    public function render($template, $params = [])
    {
        echo $this->fetch($template, $params);
    }
}

